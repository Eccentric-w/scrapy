# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SougouItem(scrapy.Item):
	dictName = scrapy.Field()
	dictUrl = scrapy.Field()