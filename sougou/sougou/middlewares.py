from scrapy import signals
from itemadapter import is_item, ItemAdapter
from fake_useragent import UserAgent

class RandomUserAgentMiddleware(object):
    def process_request(self, request, spider):
        ua = UserAgent()
        yield request.headers['User-Agent'] = ua.random
