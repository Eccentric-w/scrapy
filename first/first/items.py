# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class ItcastItem(scrapy.Item):
        name = scrapy.Field()
        level = scrapy.Field()
        info = scrapy.Field()

class MztrItem(scrapy.Item):
        name = scrapy.Field()

