# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import json

class FirstPipeline:
    def __init__(self):
        #以二进制格式只写，如果已经打开则从头编辑（文件原内容覆盖掉），如果文件不存在则创建新文件
        self.f = open("./teacher_xpath_utf-8.json","wb")    

    def process_item(self, item, spider):
        # ensure_ascii=False：的作用是：当遇到中文时，将中文当作unicode编码处理
        con = json.dumps(dict(item), ensure_ascii = False) + ",\n"
        self.f.write(con.encode("utf-8"))
        return item

    def close_spider(self,spider):
        self.f.close()
