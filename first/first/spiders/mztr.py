import scrapy
from first.items import MztrItem


class MztrSpider(scrapy.Spider):
    name = 'mztr'
    allowed_domains = ['cq.meituan.com']
    start_urls = ['http://cq.meituan.com/meishi']

    def parse(self, response):
        list = []
        names = response.xpath("//div[@class='info']")
        for name in names :
            item = MztrItem()
            uname = name.xpath('./a/h4/text()').extract()
            item['name'] = uname
            list.append(item)
        return list
