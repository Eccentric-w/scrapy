import scrapy
from first.items import ItcastItem

class ItcastSpider(scrapy.Spider):
    name = 'itcast'
    allowed_domains = ['www.itcast.cn']
    start_urls = ['http://www.itcast.cn/channel/teacher.shtml']
    headers = {
        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; AcooBrowser; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)"    
           }

    def parse(self, response):
        # lists = []
        nodes = response.xpath("//div[@class='main_mask']")
        for node in nodes :
            liem = ItcastItem()
            name = node.xpath('./h2/text()').extract()
            level = node.xpath('./h2/span/text()').extract()
            info = node.xpath('./p/text()').extract() 
            liem['name'] = name[0]
            liem['level'] = level[0]
            liem['info'] = info[0]
            yield liem
            # lists.append(liem)

        # return lists

