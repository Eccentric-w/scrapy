from stem import Signal
from stem.control import Controller

# signal TOR for a new connection 
def renew_connection():
	with Controller.from_port(port = 9051) as controller:
		controller.authenticate()
		controller.signal(Signal.NEWNYM) 

import requests
import time
import sys
import socket
import socks

socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket
print(requests.get("http://checkip.amazonaws.com").text)
# print(requests.get("http://www.baidu.com"))
renew_connection()
print(requests.get("http://checkip.amazonaws.com").text)
# print(requests.get("http://www.baidu.com"))