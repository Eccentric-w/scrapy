# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class StrItem(scrapy.Item):
	shopId = scrapy.Field()
	commentTags = scrapy.Field()
	comments = scrapy.Field()
	reviewId = scrapy.Field()

# 共12个属性
class MztrItem(scrapy.Item):
	# 餐厅id
	shopId = scrapy.Field()
	# 店名 string
	shopName = scrapy.Field()
	# 城市名字
	cityName = scrapy.Field()
	# 餐厅风格
	shopstStyle = scrapy.Field()
	# 评分 int
	avgScore = scrapy.Field()
	# 评论总数 int
	allCommentNum = scrapy.Field()
	# 餐厅地址 string
	address = scrapy.Field()
	# 平均消费 int
	avgPrice = scrapy.Field()
	# 套餐列表 list[{类型：”“，价格：int，已售：int}]
	dealList = scrapy.Field()
	# 封面图片uri string
	frontimg = scrapy.Field()
	# 电话 string
	phone = scrapy.Field()
	# 营业时间 string
	businessHours = scrapy.Field()
	# 点评标签 list [标签：”“，数量：int]
	commentTags = scrapy.Field()
	# 评论 list[内容：string，日期：string]
	comments = scrapy.Field()
	# 附加服务
	extraInfos = scrapy.Field()

class FoodItem(scrapy.Item):
	# 商品ID、名字、已售、美团价、门店价格、食物图片
	foodId = scrapy.Field()
	name = scrapy.Field()
	soldNum = scrapy.Field()
	price = scrapy.Field()
	value = scrapy.Field()
	frontImgUrl = scrapy.Field()
