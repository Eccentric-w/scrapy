# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class StrItem(scrapy.Item):
	str = scrapy.Field()

# 共12个属性
class MztrItem(scrapy.Item):
	# 餐厅id
	shopId = scrapy.Field()
	# 店名 string
	shopName = scrapy.Field()
	# 城市名字
	cityName = scrapy.Field()
	# 餐厅风格
	shopstStyle = scrapy.Field()
	# 评分 int
	avgScore = scrapy.Field()
	# 评论总数 int
	allCommentNum = scrapy.Field()
	# 餐厅地址 string
	address = scrapy.Field()
	# 平均消费 int
	avgPrice = scrapy.Field()
	# 套餐列表 list[{类型：”“，价格：int，已售：int}]
	dealList = scrapy.Field()
	# 封面图片uri string
	frontimg = scrapy.Field()
	# 电话 string
	phone = scrapy.Field()
	# 营业时间 string
	businessHours = scrapy.Field()
	# 点评标签 list [标签：”“，数量：int]
	commentTags = scrapy.Field()
	# 评论 list[内容：string，日期：string]
	comments = scrapy.Field()







# 共14个属性
# 美食杰的菜谱大全
class MeishiItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # 第一类别
    TypeOne = scrapy.Field()
    # 第二类别
    TypeTwo = scrapy.Field()
    # 食物名字
    name = scrapy.Field()
    # 制作工艺
    technology = scrapy.Field()
    # 口味
    flavor = scrapy.Field()
    # 制作时长
    makeTime = scrapy.Field()
    # 制作步数
    makeStepCount = scrapy.Field()
    # 人气
    popularity = scrapy.Field()
    # 评论数
    comment = scrapy.Field()
    # 益处
    benefit = scrapy.Field()
    # 描述
    describe = scrapy.Field()
    # 主要食材
    mainIngredients = scrapy.Field()
    # 辅助食材
    auxiliaryIngredients = scrapy.Field()
    # 图片URI
    image = scrapy.Field()
